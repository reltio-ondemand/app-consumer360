
Consumer 360 is a solution that helps manage consumer/individual information in an organization enabling profiling, and perform targeted marketing leading to effective marketing campaigns and increased sales. 


The solution is  extensible, but still approachable using the default configuration. 



## Change Log

```

Version: 2018.DDA.10
Description:

1. Updated StatusReasonCode lookup Values
2. Attribute Ordering 
3. Optimized Match Rule
4. Added SocialMedia as nested attribute
5. Updated Sample Data.
6. RDM naming standards followed.
7. Cleanser win strategy for addresses.
```

## Contributing
Please visit our [Conitributor Covenant Code of Conduct](https://bitbucket.org/reltio-ondemand/common/src/master/CodeOfConduct.md) to learn more about our contribution guidlines

## Licensing
```
Copyright (c) 2018 Reltio



Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at



    http://www.apache.org/licenses/LICENSE-2.0



Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

See the License for the specific language governing permissions and limitations under the License.
```

## Quick Start
To learn about Consumer 360 data model view these [documents](https://bitbucket.org/reltio-ondemand/app-consumer360/src//Data%20Model/?at=master).


### Repository Owners

[Ramya Krishnan](ramya.krishnan@reltio.com)

[Gregg Casey](gregg.casey@reltio.com)

[Yuriy Raskin](yuriy.raskin@reltio.com)





